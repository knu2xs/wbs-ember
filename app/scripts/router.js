WbsEmber.Router.map(function () {
  this.resource('codes', function(){

    // pill codes navigation
    this.route('prefix');
    this.route('suffix');
    this.route('single');
  });
});
