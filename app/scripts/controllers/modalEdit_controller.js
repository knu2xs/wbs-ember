// TODO: get the damn thing to recognize something other than the first record
WbsEmber.ModaleditController = Ember.ObjectController.extend({

    // add action event listeners
    actions: {

        // when the form is submitted
        submit: function() {

            // use values from form to replace existing values
            this.set('wbs', $('#wbsCode').val());
            this.set('name', $('#wbsName').val());
            this.set('type', $('#wbsType').val());

            // hide the modal
            $('#modalEdit').modal('hide');

            // commit the values to the rest endpoint/database
            // TODO: get updates to apply
            this.update();
        }
    }
});