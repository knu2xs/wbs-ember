//WbsEmber.ApplicationAdapter = DS.FixtureAdapter;

WbsEmber.ApplicationSerializer = DS.RESTSerializer.extend({
    primaryKey: '_id'
});

WbsEmber.ApplicationAdapter = DS.RESTAdapter.extend({
    host: 'http://localhost:8080',
    namespace: 'api'
});