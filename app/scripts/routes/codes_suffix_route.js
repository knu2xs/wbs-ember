WbsEmber.CodesSuffixRoute = Ember.Route.extend({
    model: function () {
        return this.modelFor('codes').filterProperty('type', 'suffix');
    }
});
