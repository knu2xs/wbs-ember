WbsEmber.CodeRoute = Ember.Route.extend({
    model: function(params) {
        return this.get('store').find('code', params.code_id);
    }
});