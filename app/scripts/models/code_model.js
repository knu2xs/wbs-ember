/*global Ember*/
WbsEmber.Code = DS.Model.extend({
    wbs: DS.attr('string'),
    name: DS.attr('string'),
    type: DS.attr('string')
});

// probably should be mixed-in...
WbsEmber.Code.reopen({
    attributes: function () {
        var model = this;
        return Ember.keys(this.get('data')).map(function (key) {
            return Em.Object.create({ model: model, key: key, valueBinding: 'model.' + key });
        });
    }.property()
});

// fixtures for testing
//WbsEmber.Code.FIXTURES = [
//    { id: 0, wbs: 'C04220', name: 'AGSS', type: 'prefix' },
//    { id: 1, wbs: 'E5180', name: "ILO", type: 'suffix' },
//    { id: 2, wbs: 'C04210', name: 'ARC1', type: 'prefix' },
//    { id: 3, wbs: 'E0040', name: 'course preparation', type: 'suffix' },
//    { id: 4, wbs: 'C04232', name: 'ARC2', type: 'prefix' },
//    { id: 5, wbs: 'E0041', name: 'learn new course', type: 'suffix' },
//    { id: 6, wbs: '4710', name: 'instructor labor', type: 'single' },
//    { id: 7, wbs: 'ES1028', name: 'tech lead labor', type: 'single' }
//];
