# WBS-Ember

If you know what a WBS Code is, you will love this project. If you do not, it will appear rather irrelevant. WBS Codes are cryptic codes created by evil wizards. Every employee of a certain very prominent geospatial software company, at the end of every day, must look up these codes to account for what transpired during their day...to bill, or at least track, their time. There should be a charge code for looking up charge codes. Finding these stupid things can absolutely drive a person crazy. Enter the wbs-ember and the wbs-api project.

This project is rather simple. The first page embeds in it the timesheet application created in 1994. Incidentally, this application likely was created in 1994 and has not been touched since. At the top of the page is a search, enabling you to find the right code, then copy and paste it into the embedded timesheet application. It is not perfect, but it does make it easier.

The second page is for adding, updating and removing unneeded codes. These codes are stored in a database most likely located on the same server hosting the wbs-ember application. The projects, not surprisingly, are designed to work together. Hence, to make wbs-ember work, you will need a REST endpoint created by the wbs-api project.

## Current Status (Version 0.0.5)

Right now this thing is not even close to being functionally complete. All it can do is display what it finds at the REST endpoint. However, it does connect to the REST endpoint. We are well on our way to CRUD!